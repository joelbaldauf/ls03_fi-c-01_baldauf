package de.oszimt.ls.aliendefence.view;

import de.oszimt.ls.aliendefence.controller.AlienDefenceController;
import de.oszimt.ls.aliendefence.model.persistence.IPersistance;
import de.oszimt.ls.aliendefence.model.persistence.IUserPersistance;
import de.oszimt.ls.aliendefence.model.persistenceDB.AccessDB;
import de.oszimt.ls.aliendefence.model.persistenceDB.PersistanceDB;
import de.oszimt.ls.aliendefence.model.persistenceDB.UserDB;
import de.oszimt.ls.aliendefence.view.menue.MainMenu;

public class StartAlienDefence {

	public static void main(String[] args) {
		AccessDB               dbAccess               = new AccessDB();
		IPersistance 		   alienDefenceModel      = new PersistanceDB();
		IUserPersistance       userModel              = new UserDB(dbAccess);
		AlienDefenceController alienDefenceController = new AlienDefenceController(alienDefenceModel, userModel);
		MainMenu.show(alienDefenceController);
	}
}
