package de.oszimt.ls.aliendefence.view.menue;

import de.oszimt.ls.aliendefence.controller.AlienDefenceController;
import de.oszimt.ls.aliendefence.controller.GameController;
import de.oszimt.ls.aliendefence.controller.LevelController;
import de.oszimt.ls.aliendefence.model.Level;
import de.oszimt.ls.aliendefence.model.User;
import de.oszimt.ls.aliendefence.view.game.GameGUI;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class LevelChoice {
    private JPanel panel;
    private JButton btnNewLevel;
    private JButton btnUpdateLevel;
    private JTable tblLevels;
    private JButton btnDeleteLevel;
    private JButton btnPlay;
    private JScrollPane scrollPane;
    private JButton btnBack;
    private JButton btnHighscore;

    private final LevelController lvlControl;
    private final LeveldesignWindow leveldesignWindow;
    private DefaultTableModel jTableData;

    /**
     * Create the panel
     * @param controller
     * @param leveldesignWindow
     * @param source
     */
    public LevelChoice(AlienDefenceController controller, LeveldesignWindow leveldesignWindow, User user, int source) {
        this.lvlControl = controller.getLevelController();
        this.leveldesignWindow = leveldesignWindow;

        btnNewLevel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                btnNewLevel_Clicked();
            }
        });

        btnUpdateLevel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnUpdateLevel_Clicked();
            }
        });

        btnDeleteLevel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                btnDeleteLevel_Clicked();
            }
        });

        tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        this.updateTableData();
        btnPlay.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnPlay_Clicked(controller, user);
            }
        });

        if (source == LeveldesignWindow.LEVELEDITOR) {
            btnPlay.setVisible(false);
            btnHighscore.setVisible(false);
        }
        else if (source == LeveldesignWindow.TESTEN) {
            btnNewLevel.setVisible(false);
            btnUpdateLevel.setVisible(false);
            btnDeleteLevel.setVisible(false);
        }
        scrollPane.getViewport().setBackground(Color.BLACK);

        tblLevels.getTableHeader().setOpaque(false);
        tblLevels.getTableHeader().setBackground(Color.BLACK);
        tblLevels.getTableHeader().setForeground(Color.YELLOW);
        btnBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                leveldesignWindow.dispose();
            }
        });
        btnHighscore.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnHighscore_Clicked(controller);
            }
        });
    }

    private String[][] getLevelsAsTableModel() {
        List<Level> levels = this.lvlControl.readAllLevels();
        String[][] result = new String[levels.size()][];
        int i = 0;
        for (Level l : levels) {
            result[i++] = l.getData();
        }
        return result;
    }

    public void updateTableData() {
        this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
        this.tblLevels.setModel(jTableData);
    }

    public void btnNewLevel_Clicked() {
        this.leveldesignWindow.startLevelEditor();
    }

    public void btnUpdateLevel_Clicked() {
        int level_id = Integer
                .parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
        this.leveldesignWindow.startLevelEditor(level_id);
    }

    public void btnDeleteLevel_Clicked() {
        int level_id = Integer
                .parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
        this.lvlControl.deleteLevel(level_id);
        this.updateTableData();
    }

    public void btnPlay_Clicked(AlienDefenceController alienDefenceController, User user) {
        //Level_id des selektierten Elements auslesen
        int level_id = Integer
                .parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
        Thread t = new Thread("GameThread") {
            @Override
            public void run() {
                GameController gameController = alienDefenceController.startGame(alienDefenceController.getLevelController().readLevel(level_id), user);
                new GameGUI(gameController).start();
            }
        };
        t.start();
    }

    public void btnHighscore_Clicked(AlienDefenceController alienDefenceController) {
        //Level_id des selektierten Elements auslesen
        int level_id = Integer
                .parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));

        new Highscore(alienDefenceController.getAttemptController(), alienDefenceController.getLevelController().readLevel(level_id));
    }


    public JPanel getPanel() {
        return panel;
    }
}
