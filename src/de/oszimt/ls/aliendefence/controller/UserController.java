package de.oszimt.ls.aliendefence.controller;

import de.oszimt.ls.aliendefence.model.User;
import de.oszimt.ls.aliendefence.model.persistence.IUserPersistance;

/**
 * de.oszimt.ls.aliendefence.controller for users
 * @author Joel Baldauf
 */
public class UserController {

	private IUserPersistance userPersistance;
	
	public UserController(IUserPersistance userPersistance) {
		this.userPersistance = userPersistance;
	}

	/**
	 * erstellt einen User in der Persistenzschicht
	 * @param user Das zu erstellende Userobjekt
	 *
	 */
	public void createUser(User user) {
		this.userPersistance.createUser(user);
	}
	
	/**
	 * liest einen User aus der Persistenzschicht und gibt das Userobjekt zurück
	 * @param username eindeutige Loginname
	 * @param passwort das richtige Passwort
	 * @return Userobjekt, null wenn der User nicht existiert
	 */

	public User readUser(String username, String passwort) {
		if (checkPassword(username, passwort)) return this.userPersistance.readUser(username);
		else return null;
	}

	/**
	 * verändert einen User in der Persistenzschicht
	 * @param user zu aktualisierender User
	 *
	 */
	public void changeUser(User user) {
		this.userPersistance.updateUser(user);
	}

	/**
	 * löscht einen User in der Persistenzschicht
	 * @param user zu löschender User
	 *
	 */
	public void deleteUser(User user) {
		this.userPersistance.deleteUser(user);
	}

	/**
	 * überprüft, ob das Passwort eines Benutzers richtig ist
	 * @param username zu prüfender Nutzername
	 * @param passwort zu prüfendes Passwort
	 */
	public boolean checkPassword(String username, String passwort) {
		return this.userPersistance.readUser(username).getPassword().equals(passwort);
	}
}
