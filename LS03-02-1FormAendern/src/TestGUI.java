import javax.swing.*;
import java.awt.*;

public class TestGUI {
    private JPanel panel1;
    private JButton rotButton;
    private JButton gruenButton;
    private JButton blauButton;
    private JButton gelbButton;
    private JButton standardfarbeButton;
    private JButton farbeWechselnButton;
    private JTextField hierBitteTextEingebenTextField;
    private JButton arialButton;
    private JButton comicSansButton;
    private JButton courierNewButton;
    private JButton writeToLabelButton;
    private JButton clearLabelButton;
    private JLabel lblTextAnzeige;
    private JButton fontRedBtn;
    private JButton fontBlueBtn;
    private JButton fontBlackBtn;
    private JButton maxFontBtn;
    private JButton minFontBtn;
    private JButton leftBtn;
    private JButton centerBtn;
    private JButton rightBtn;
    private JButton exitBtn;

    public TestGUI() {
        rotButton.addActionListener(e -> panel1.setBackground(Color.red));
        gruenButton.addActionListener(e -> panel1.setBackground(Color.green));
        blauButton.addActionListener(e -> panel1.setBackground(Color.blue));
        gelbButton.addActionListener(e -> panel1.setBackground(Color.yellow));
        standardfarbeButton.addActionListener(e -> panel1.setBackground(null));
        farbeWechselnButton.addActionListener(e -> panel1.setBackground(JColorChooser.showDialog(null, "Farbe auswählen", Color.white)));
        arialButton.addActionListener(e -> lblTextAnzeige.setFont(new Font("Arial", Font.PLAIN, lblTextAnzeige.getFont().getSize())));
        comicSansButton.addActionListener(e -> lblTextAnzeige.setFont(new Font("Comic Sans MS", Font.PLAIN, lblTextAnzeige.getFont().getSize())));
        courierNewButton.addActionListener(e -> lblTextAnzeige.setFont(new Font("Courier New", Font.PLAIN, lblTextAnzeige.getFont().getSize())));
        writeToLabelButton.addActionListener(e -> lblTextAnzeige.setText(hierBitteTextEingebenTextField.getText()));
        clearLabelButton.addActionListener(e -> lblTextAnzeige.setText(""));
        fontRedBtn.addActionListener(e -> lblTextAnzeige.setForeground(Color.red));
        fontBlueBtn.addActionListener(e -> lblTextAnzeige.setForeground(Color.blue));
        fontBlackBtn.addActionListener(e -> lblTextAnzeige.setForeground(Color.black));
        maxFontBtn.addActionListener(e -> lblTextAnzeige.setFont(new Font(lblTextAnzeige.getFont().getName(), Font.PLAIN, lblTextAnzeige.getFont().getSize()+1)));
        minFontBtn.addActionListener(e -> lblTextAnzeige.setFont(new Font(lblTextAnzeige.getFont().getName(), Font.PLAIN, lblTextAnzeige.getFont().getSize()-1)));
        leftBtn.addActionListener(e -> lblTextAnzeige.setHorizontalAlignment(SwingConstants.LEFT));
        centerBtn.addActionListener(e -> lblTextAnzeige.setHorizontalAlignment(SwingConstants.CENTER));
        rightBtn.addActionListener(e -> lblTextAnzeige.setHorizontalAlignment(SwingConstants.RIGHT));
        exitBtn.addActionListener(e -> System.exit(1));
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("TestGUI");
        frame.setContentPane(new TestGUI().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
